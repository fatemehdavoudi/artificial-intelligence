from collections import deque
import time
import heapq

class Node:
    def __init__(self, state, parent, action, path_cost):
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = path_cost
        self.fn = 0

    def __lt__(self, other):
        if self.fn < other.fn:
            return True
        return False

    def __gt__(self, other):
        if self.fn > other.fn:
            return True
        return False

def actions(state, table, n, m, c):
    location = state[0]
    balls_on_table = state[1]
    balls_in_pack = state[2]
    index = -1
    possible_actions = list()
    if len(balls_in_pack) < c: #Pick up a Ball
        for i in range(len(balls_on_table)):
            if location == (balls_on_table[i][0], balls_on_table[i][1]):
                possible_actions.append('P')
                index = i
                break
    if bool(balls_in_pack): #Put the Ball on the Table
        for i in range(len(balls_in_pack)):
            if location == (balls_in_pack[i][2], balls_in_pack[i][3]):
                balls_in_pack = balls_in_pack[:i] + balls_in_pack[i+1:]
                break
    if location[1] + 1 < m: #Move to Right
        if table[location[0]][location[1] + 1] != '*':
            possible_actions.append('R')
    if location[1] - 1 >= 0: #Move to Left
        if table[location[0]][location[1] - 1] != '*':
            possible_actions.append('L')
    if location[0] - 1 >= 0: #Move to Up
        if table[location[0] - 1][location[1]] != '*':
            possible_actions.append('U')
    if location[0] + 1 < n: #Move to Down
        if table[location[0] + 1][location[1]] != '*':
            possible_actions.append('D')
    return possible_actions, balls_in_pack, index

def create_new_node(node, action, index):
    parent = node
    path_cost = node.path_cost + 1
    location = node.state[0]
    balls_on_table = node.state[1]
    balls_in_pack = node.state[2]
    if action == 'P':
        balls_in_pack = balls_in_pack + (balls_on_table[index],)
        balls_on_table = balls_on_table[:index] + balls_on_table[index+1:]
    if action == 'R':
        location = list(location)
        location[1] = location[1] + 1
    if action == 'L':
        location = list(location)
        location[1] = location[1] - 1
    if action == 'U':
        location = list(location)
        location[0] = location[0] - 1
    if action == 'D':
        location = list(location)
        location[0] = location[0] + 1
    new_node = Node((tuple(location), balls_on_table, balls_in_pack), parent, action, path_cost)
    return new_node

def goal_test(state, end_point):
    location = list(state[0])
    balls_on_table = state[1]
    balls_in_pack = state[2]
    if location[0] == end_point[0] and location[1] == end_point[1] and len(balls_in_pack) == 0 and len(balls_on_table) == 0:
        return True
    return False

def to_find_path(node):
    path = list()
    while node.parent != -1:
        path.append(node.action)
        node = node.parent
    path.reverse()
    return path

def BFS(state, table, n, m, c, end_point):
    number_of_states = 1
    node = Node(state, -1, '', 0)
    if goal_test(state, end_point) == True:
        return node, 1, 1
    frontier = deque()
    frontier.append(node)
    explored = set()
    explored.add(node)
    while bool(frontier) == True:
        node = frontier.popleft()
        possible_actions, balls_in_pack, index = actions(node.state, table, n, m, c)
        node.state = (node.state[0], node.state[1], balls_in_pack)
        for action in possible_actions:
            child_node = create_new_node(node, action, index)
            number_of_states += 1
            if goal_test(child_node.state, end_point):
                return child_node, len(explored), number_of_states
            if (child_node.state in explored) == False:
                frontier.append(child_node)
                explored.add(child_node.state)
    return 0, len(explored), number_of_states

def DLS(state, table, n, m, c, end_point, limit, unique_states):
    number_of_states = 1
    node = Node(state, -1, '', 0)
    unique_states.add(node.state)
    if goal_test(state, end_point) == True:
        return node, 1
    frontier = deque()
    frontier.append(node)
    seen_states = set()
    seen_states.add((node.state, 0))
    while bool(frontier) == True:
        node = frontier.pop()
        possible_actions, balls_in_pack, index = actions(node.state, table, n, m, c)
        node.state = (node.state[0], node.state[1], balls_in_pack)
        if goal_test(node.state, end_point):
            return node, number_of_states
        if node.path_cost < limit:
            for action in possible_actions:
                child_node = create_new_node(node, action, index)
                number_of_states += 1
                if (node.path_cost < limit):
                    if (((child_node.state, child_node.path_cost) in seen_states) == False):
                        frontier.append(child_node)
                        unique_states.add(child_node.state)
                        for i in range(node.path_cost, limit):
                            seen_states.add((child_node.state, i))
    return 0, number_of_states

def IDS(state, table, n, m, c, end_point):
    depth = 0
    unique_states = set()
    number_of_states = 0
    result = 0
    while result == 0:
        result, i = DLS(state, table, n, m, c, end_point, depth, unique_states)
        number_of_states += i
        depth += 1
    return result, len(unique_states), number_of_states

def h1(n, m, node):
    return len(node.state[1]) + len(node.state[2])

def h2(n, m, node):
    location = list(state[0])
    balls_on_table = state[1]
    min_dist = m + n
    for i in range(len(balls_on_table)):
        temp = abs(location[0] - balls_on_table[i][0]) + abs(location[1] - balls_on_table[i][1])
        if temp < min_dist:
            min_dist = temp
    return min_dist

def AStar(state, table, n, m, c, end_point, alpha, h):
    number_of_states = 1
    node = Node(state, -1, "", 0)
    if goal_test(node.state, end_point):
        return node, 1, 1
    frontier = list()
    node.fn = node.path_cost + alpha * h(n, m, node)
    frontier.append(node)
    heapq.heapify(frontier)
    explored = set()
    explored.add(node.state)
    while bool(frontier) == True:
        node = heapq.heappop(frontier)
        possible_actions, balls_in_pack, index = actions(node.state, table, n, m, c)
        node.state = (node.state[0], node.state[1], balls_in_pack)
        for action in possible_actions:
            child_node = create_new_node(node, action, index)
            child_node.fn = child_node.path_cost + alpha * h(n, m, child_node)
            number_of_states += 1
            if goal_test(child_node.state, end_point):
                return child_node, len(explored), number_of_states
            if (child_node.state in explored) == False:
                heapq.heappush(frontier, child_node)
                explored.add(child_node.state)
    return 0, len(explored), number_of_states

def print_output(tic, toc, node, number_of_unique_states, number_of_states):
    print("time: ", toc - tic)
    print("number_of_states: ", number_of_states)
    print("number_of_unique_states: ", number_of_unique_states)
    print("path cost: ", node.path_cost)
    print("path: ", to_find_path(node))

def get_input():
    n, m = [int(x) for x in input().split()]
    start_point = [int(x) for x in input().split()]
    end_point = [int(x) for x in input().split()]
    c = int(input())
    k = int(input())
    balls = tuple()
    for i in range(k):
        temp = tuple([int(x) for x in input().split()])
        balls = balls + (temp,)
    table = list()
    for i in range(n):
        temp = input().split()
        table.append(temp)
    return n, m, start_point, end_point, c, k, balls, table

n, m, start_point, end_point, c, k, balls, table = get_input()
balls_in_pack = tuple()
state = (tuple(start_point), balls, balls_in_pack)

print("*****BFS*****")
tic = time.time()
node, number_of_unique_states, number_of_states = BFS(state, table, n, m, c, end_point)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)

print("*****IDS*****")
tic = time.time()
node, number_of_unique_states, number_of_states = IDS(state, table, n, m, c, end_point)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)

print("*****AStar-h1*****")
tic = time.time()
node, number_of_unique_states, number_of_states = AStar(state, table, n, m, c, end_point, 1, h1)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)

print("*****AStar-h2*****")
tic = time.time()
node, number_of_unique_states, number_of_states = AStar(state, table, n, m, c, end_point, 1, h2)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)

print("*****weighted AStar-h2-alpha=5*****")
tic = time.time()
node, number_of_unique_states, number_of_states = AStar(state, table, n, m, c, end_point, 5, h2)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)

print("*****weighted AStar-h1-alpha=10000*****")
tic = time.time()
node, number_of_unique_states, number_of_states = AStar(state, table, n, m, c, end_point, 10000, h1)
toc = time.time()
print_output(tic, toc, node, number_of_unique_states, number_of_states)
